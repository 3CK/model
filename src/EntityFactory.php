<?php
declare(strict_types=1);

namespace Trick\Model;

class EntityFactory implements IEntityFactory
{
	private $entityClass;


	public function __construct(string $entityClass)
	{
		$this->entityClass = $entityClass;
	}


	public function create($data): Entity
	{
		return new $this->entityClass($data);
	}
}
