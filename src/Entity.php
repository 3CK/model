<?php
declare(strict_types=1);

namespace Trick\Model;

use Trick\Model\LazyLoad\LazyLoadBox;
use Trick\Model\Localization\LocalizedProperty;

/**
 * @property array $formValues
 */
abstract class Entity
{
	use \Nette\SmartObject {
		__get as public traitGet;
	}

	/** @var LazyLoadBox */
	protected $lazyload;

	/** @var string[] */
	protected $autowiredProperties = [];

	/** @var string[] */
	protected $foreignKeys = [];

	/** @var string[] */
	protected $imageSets = [];

	/** @var string[] */
	protected $translatedProperties = [];

	/** @var string[] */
	protected $locales = [];


	public function __construct($row, array $locales = [])
	{
		$this->locales = $locales;

		$reflection = new \ReflectionClass(get_class($this));
		$this->lazyload = new LazyLoadBox($reflection);

		$this->parseProperties();
		$this->loadAutowiredProperties($row);
	}


	private function parseProperties(): void
	{
		$class = get_class($this);

		foreach (get_class_vars($class) as $name => $foo) {
			$rp = new \ReflectionProperty($class, $name);
			$comment = $rp->getDocComment();
			if ($comment) {
				if (preg_match('/@autowired/', $comment)) {
					$this->autowiredProperties[] = $name;
				}
				if (preg_match('/@foreignKey/', $comment)) {
					$this->foreignKeys[] = $name;
				} else if (preg_match('/@lazyload/', $comment)) {
					$this->lazyload->addProperty($name);
				} else if (preg_match('/ImageSet/', $comment)) {
					$this->imageSets[] = $name;
				} else if (preg_match('/@translated/', $comment)) {
					$this->translatedProperties[] = $name;
				}
			}
		}
	}


	public function addLazyloader(string $property, Callable $loader): void
	{
		$this->lazyload->addLoader($property, $loader);
	}


	private function loadAutowiredProperties($dataSource): void
	{
		foreach ($this->autowiredProperties as $name) {
			try {
				$dbName = $this->nameToDatabaseFormat($name);

				if (in_array($name, $this->translatedProperties)) {
					$property = new LocalizedProperty($name);

					foreach ($this->locales as $locale) {
						$localeName = $dbName . '_' . $locale;
						$property->addTranslation($locale, $dataSource->$localeName);
					}

					$this->$name = $property;
				} else {
					if (in_array($name, $this->foreignKeys)) {
						$dbName = preg_replace('/_id$/', '', $dbName);
					}

					$this->$name = $dataSource->$dbName;
				}
			} catch (\Nette\MemberAccessException $e) {
				throw new \Exception('Property "' . $dbName . '" not found in a database source (' . get_class($this) . ').');
			}
		}
	}


	public function &__get($name)
	{
		if ($this->lazyload->isLazyloaded($name)) {
			$value = $this->lazyload->get($name);
			return $value;
		} else {
			return $this->traitGet($name);
		}
	}

	protected function nameToDatabaseFormat(string $name): string
	{
		return strtolower(preg_replace('/\B([A-Z])/', '_$1', $name));
	}

	public function setCurrentLocale(string $locale): void
	{
		foreach ($this->translatedProperties as $property) {
			$this->$property->setLocale($locale);
		}
	}

	public function __sleep()
	{
		return array_merge($this->autowiredProperties, $this->imageSets, ['translatedProperties', 'locales', 'foreignKeys', 'autowiredProperties', 'lazyload']);
	}


    /**
	 * @param array<string> $filterLazyloadedProperties
     * @return array<mixed>
     **/
	public function toArray(bool $includeLazyloadedProperties = false, array $filterLazyloadedProperties = [], bool $deep = false): array
	{
		$values = [];

		foreach ($this->autowiredProperties as $property) {
			$values[$property] = $this->$property;
		}

		if ($includeLazyloadedProperties) {
			foreach ($this->lazyload->getPropertiesNames() as $name) {
				if (empty($filterLazyloadedProperties) || in_array($name, $filterLazyloadedProperties)) {
					$property = $this->lazyload->get($name);

					if (is_array($property)) {
						$propertyArrayed = [];

						foreach ($property as $item) {
							$propertyArrayed[] = $item->toArray($deep);
						}

						$values[$name] = $propertyArrayed;
					} else if ($property && method_exists($property, 'toArray')) {
						$values[$name] = $property->toArray($deep);
					} else {
						continue;
					}
				}
			}
		}

		return $values;
	}
}
