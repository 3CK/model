<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette;
use Nette\Database\Context;
use Nette\Utils\ArrayHash;

abstract class Repository
{
	use Nette\SmartObject;

	/** @var Context */
	protected $db;

	/** @var string */
	protected $tableName;


	public function __construct(Context $database)
	{
		$this->db = $database;
	}


	protected function getTableName()
	{
		if (!$this->tableName) {
			preg_match('#(\w+)$#', get_class($this), $m);
			$name = preg_replace('/([A-Z])/', '_$1', lcfirst($m[1]));
			$this->tableName = lcfirst($name);
		}

		return $this->tableName;
	}


	public function get(Filter $filter = null)
	{
		$data = $this->getTable();

		if ($filter) {
			$filter->applyTo($data);
		}

		return $data;
	}


	public function getTable(string $tableName = null)
	{
		return $this->db->table($tableName ?: $this->getTableName());
	}


	/** @return Nette\Database\Table\ActiveRow */
	public function getById($id)
	{
		return $this->getTable()->get($id);
	}


	public function save(&$values)
	{
		if (is_array($values)) {
			$values = ArrayHash::from($values);
		}

		if (isset($values->id) && $values->id) {
			$this->getTable()->where('id', $values->id)->update($values);
		} else {
			$values->id = null;
			$values->id = $this->getTable()->insert($values)->id;
		}

		return $values->id;
	}


	/** Insert only, no update!! */
	public function multiSave(&$values)
	{
		$this->getTable()->insert($values);
	}


	public function remove(int $id): void
	{
		$this->getTable()->where('id', $id)->delete();
	}


	public function removeWhere(array $where): void
	{
		$this->getTable()->where($where)->delete();
	}


	public function count(Filter $filter = null): int
	{
		return $this->get($filter)->count();
	}


	/* @TODO: Predelat na jeden prikaz nejak? neslo by? */
	public function saveOrder(array $ids): void
	{
		foreach ($ids as $order => $id) {
			$this->findAll()->where('id', $id)->update(['order' => $order]);
		}
	}
}
