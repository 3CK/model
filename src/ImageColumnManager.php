<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette\Http\FileUpload;
use Nette\Utils\ArrayHash;
use Trick\ImageManager\ImageManager;

trait ImageColumnManager
{
	/** @var ImageManager */
	private $imageManager;

	/** @var array */
	private $imageSettings;


	protected function uploadImage(FileUpload &$image, string $name = null): ?string
	{
		if ($image && $image->isOk()) {
			return $this->imageManager->upload($image, $this->imageSettings, $name);
		} else {
			return null;
		}
	}


	protected function removeImage(ArrayHash $values, string $column): void
	{
		if (isset($values->$column) && $values->$column && $values->$column->isOk() && $values->id) {
			$oldImage = $this->getById((int) $values->id)->$column;

			if ($oldImage) {
				$this->imageManager->remove($oldImage);
			}
		}
	}


	protected function removeImageFiles(int $entityId, string $property): void
	{
		$image = $this->getById($entityId)->$property;
		if ($image) {
			$this->imageManager->remove($image);
		}
	}
}
