<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette\MemberAccessException;


abstract class Facade
{
	use \Nette\SmartObject;

	public function __call($method, $args)
	{
		$class = get_class($this);
		$path = explode('\\', $class);
		$var = str_replace('Facade', '', end($path));;
		$var = lcfirst($var);

		if (method_exists($this->$var, $method)) {
			if ($method == 'save') {
				$values = $args[0];
				$args = [&$values];
			}

			return call_user_func_array([$this->$var, $method], $args);
		} else {
			throw new MemberAccessException('Call to undefined method ' . $class . '::' . $method . '().');
		}
	}
}
