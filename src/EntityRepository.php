<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette;
use Nette\Utils\ArrayHash;
use Nette\Database;
use Nette\Database\Row;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;
use Nette\DI\Container;
use Nette\DI\MissingServiceException;

abstract class EntityRepository
{
	use Nette\SmartObject;

	public const TABLE_PREFIX = '';

	/** @var bool */
	public $usePluralTableName = false;

	/** @var Nette\Database\Context */
	protected $db;

	/** @var string */
	protected $tableName;

	/** @var array Already loaded entities store. */
	protected $runtimeCache = [];

	/** @var string */
	protected $entityClass;

	protected ?IEntityFactory $entityFactory = null;

	/** @var \Nette\DI\Container */
	protected $container;

	public function __construct(Container $container)
	{
		$this->db = $container->getByType('\Nette\Database\Context');
		$this->container = $container;

		$class = get_class($this);
		$r = new \ReflectionClass($class);
		$comment = $r->getDocComment();

		if ($comment) {
			if (preg_match('#@table\s+([a-zA-Z0-9_-]+)#', $comment, $matches)) {
				$this->tableName = $matches[1];
			}
			if (preg_match('#@entity\s+([a-zA-Z0-9_\\\-]+)#', $comment, $matches)) {
				$this->entityClass = $matches[1];
			}
			if (preg_match('#@entityFactory\s+([a-zA-Z0-9_\\\-]+)#', $comment, $matches)) {
				$this->entityFactory = $this->container->getByType($matches[1]);
			}
		}
	}


	public function getTable(string $name = null): Database\Table\Selection
	{
		return $this->db->table($name ?: $this->getTableName());
	}


	/**
	 * @return array<Entity>
	 */
	public function get(Filter $filter = null)
	{
		$data = $this->getTable();

		if ($filter) {
			$filter->applyTo($data);
		}

		return $this->createEntities($data);
	}


	public function getSingle(Filter $filter = null): ?Entity
	{
		if (!$filter) {
			$filter = new Filter;
		}

		$filter->limit(1);

		$found = $this->get($filter);
		return array_pop($found);
	}


	public function getById(int $id): ?Entity
	{
		if (isset($this->runtimeCache[$id])) {
			return $this->runtimeCache[$id];
		} else {
			$filter = (new Filter)->where('id', $id);
			$entities = $this->get($filter);
			return array_pop($entities);
		}
	}


	/**
	 * @param ArrayHash|array<mixed> $values
	 */
	public function save(&$values): int
	{
		if (is_array($values)) {
			$values = ArrayHash::from($values);
		}

		if (isset($values->id) && $values->id) {
			$this->getTable()->where('id', $values->id)->update($values);
			$id = $values->id;
		} else {
			unset($values->id);
			$id = $this->getTable()->insert($values)->id;
		}

		$values->id = (int) $id;

		$this->invalidate($values->id);

		return $values->id;
	}


	public function remove(int $id): void
	{
		if (isset($this->runtimeCache[$id])) {
			unset($this->runtimeCache[$id]);
		}

		$this->getTable()->where('id', $id)->delete();
	}


	public function removeWhere(array $where): void
	{
		$filter = new Filter;
		$filter->where($where);

		$ids = array_keys($this->get($filter));

		foreach ($ids as $id) {
			$this->remove($id);
		}
	}


	public function count(Filter $filter = null): int
	{
		$data = $this->getTable();

		if ($filter) {
			$filter->applyTo($data);
		}

		return $data->count();
	}

	/**
	 * @param int[] $ids
	 */
	public function saveOrder(array $ids): void
	{
		foreach ($ids as $order => $id) {
			$this->getTable()->where('id', $id)->update(['order' => $order]);
		}
	}

	/**
	 * @param Row[]|ActiveRow[]|Selection $rows
	 * @return array<Entity>
	 */
	protected function createEntities(mixed $rows): array
	{
		$entities = [];
		foreach ($rows as $row) {
			$id = $row->id;
			if (!isset($this->runtimeCache[$id])) {
				$factory = $this->getEntityFactory();
				$entity = $factory->create($row);

				$onWakeUpMethod = "onWakeUp";
				if (method_exists($factory, $onWakeUpMethod)) {
					$factory->$onWakeUpMethod($entity);
				}

				$this->runtimeCache[$id] = $entity;
			}

			$entities[$id] = $this->runtimeCache[$id];
		}

		return $entities;
	}

	protected function getTableName(): string
	{
		if (!$this->tableName) {
			preg_match('#(\w+)$#', get_class($this), $m);
			$name = preg_replace('/([A-Z])/', '_$1', lcfirst($m[1]));

			if (!$this->usePluralTableName) {
				if (substr($name, strlen($name) - 3, 3) === 'ies') {
					$name = substr($name, 0, -3) . 'y';
				} elseif (substr($name, strlen($name) - 1, 1) === 's') {
					$name = substr($name, 0, -1);
				}
			}

			$this->tableName = self::TABLE_PREFIX . mb_strtolower($name);
		}

		return $this->tableName;
	}

	protected function getEntityClassName(): string
	{
		if (!$this->entityClass) {
			$name = get_class($this);

			if (substr($name, strlen($name) - 3, 3) === 'ies') {
				$name = substr($name, 0, -3) . 'y';
			} elseif (substr($name, strlen($name) - 1, 1) === 's') {
				$name = substr($name, 0, -1);
			}

			$this->entityClass = $name;
		}

		return $this->entityClass;
	}


	protected function getEntityFactory(): IEntityFactory
	{
		if (!$this->entityFactory) {
			$factory = $this->getEntityClassName() . 'Factory';

			try {
				$this->entityFactory = $this->container->getByType($factory);
			} catch (MissingServiceException $e) {
				$this->entityFactory = new EntityFactory($this->getEntityClassName());
			}
		}

		return $this->entityFactory;
	}


	/**
	 * Invalidates cached entities in runtime variable.
	 *
	 * @param array of ints | int | NULL $ids IDs of entities to remove. If null, all entities are removed.
	 * @return void
	 */
	public function invalidate($ids = null): void
	{
		if ($ids === null) {
			$this->runtimeCache = [];
		} else {
			$ids = is_array($ids) ? $ids : [$ids];

			foreach ($ids as $id) {
				if (isset($this->runtimeCache[$id])) {
					unset($this->runtimeCache[$id]);
				}
			}
		}
	}


}
