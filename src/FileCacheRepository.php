<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette\Caching;
use Nette\DI\Container;

abstract class FileCacheRepository extends EntityRepository
{
	private $cache;


	public function __construct(Container $container, Caching\IStorage $storage)
	{
		parent::__construct($container);

        $name = str_replace('\\', '_', $this->getEntityClassName());
        $this->cache = new Caching\Cache($storage, $name);
	}


	public function get(Filter $filter = null): array
	{
		$data = $this->getTable()->select($this->tableName . '.id');

		if ($filter) {
			$filter->applyTo($data);
		}

		$ids = $data->fetchPairs('id', 'id');

		return $this->createEntities($ids);
	}


	public function getById(int $id): ?Entity
	{
		if (isset($this->runtimeCache[$id])) {
			return $this->runtimeCache[$id];
		} else {
			$entities = $this->createEntities([$id]);
			return array_pop($entities);
		}
	}


	public function save(&$values): int
	{
		parent::save($values);
		$this->invalidate($values->id);
		return $values->id;
	}


	public function remove(int $id): void
	{
		parent::remove($id);
		$this->invalidate($id);
	}


	public function removeWhere(array $where): void
	{
		$filter = new Filter;
		$filter->where($where);

		$ids = array_keys($this->get($filter));

		foreach ($ids as $id) {
			$this->remove($id);
		}
	}


	public function saveOrder(array $ids): void
	{
		parent::saveOrder($ids);
		$this->invalidate($ids);
	}


	protected function createEntities($ids): array
	{
		$entities = [];

		foreach ($ids as $id) {
			if (!isset($this->runtimeCache[$id])) {
				$entity = $this->cache->load($id, function () use ($id) {
					return $this->createEntity($id);
				});

				if (method_exists($this->getEntityFactory(), 'onWakeUp')) {
					$this->getEntityFactory()->onWakeUp($entity);
				}


				$this->runtimeCache[$id] = $entity;
			}

			$entities[$id] = $this->runtimeCache[$id];
		}

		return $entities;
	}


	private function createEntity(int $id): Entity
	{
		$row = $this->getTable()->where('id', $id)->fetch();

		if ($row === null) {
			throw new \Exception('Entity ' . $this->getEntityClassName() . ' with id ' . $id . ' not found.');
		}

		return $this->getEntityFactory()->create($row);
	}


	/**
	 * Invalidates cached products - in file and in runtime variable.
	 *
	 * @param array of ints | int | NULL $ids IDs of products to remove. If null, all products are removed.
	 * @return void
	 */
	public function invalidate($ids = null): void
	{
		if ($ids === null) {
			$this->cache->clean([Caching\Cache::ALL => true]);
			$this->runtimeCache = [];
		} else {
			$ids = is_array($ids) ? $ids : [$ids];

			foreach ($ids as $id) {
				$this->cache->remove($id);

				if (isset($this->runtimeCache[$id])) {
					unset($this->runtimeCache[$id]);
				}
			}
		}
	}
}
