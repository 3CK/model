<?php
declare(strict_types=1);

namespace Trick\Model\LazyLoad;

final class LazyLoadBox
{
	/** @var array<Property> */
	private $properties = [];


	public function addLoader(string $property, Callable $loader): void
	{
		if (!array_key_exists($property, $this->properties)) {
			throw new \LogicException('Trying to add new loader to an undefined lazy-loaded property "' . $property . '". Did you add @lazyload annotation in entity?');
		}

		$this->properties[$property]->loader = $loader;
	}

	/**
	 * Is property marked as @lazyload?
	 */
	public function isLazyloaded(string $name): bool
	{
		return array_key_exists($name, $this->properties);
	}

	public function get(string $name)
	{
		if (!$this->properties[$name]->loader) {
			throw new \Exception('Missing lazyloader for property "' . $name .'".');
		}

		$prop = &$this->properties[$name];

		if (!$prop->isLoaded) {
			$prop->data = ($this->properties[$name]->loader)();
			$prop->isLoaded = true;
		}

		return $prop->data;
	}

	public function addProperty(string $name): void
	{
		$property = new LazyLoadedProperty;
		$property->name = $name;
		$this->properties[$name] = $property;
	}

	public function __sleep()
	{
		foreach ($this->properties as &$property) {
			$property->data = null;
			$property->isLoaded = false;
			$property->loader = null;
		}

		return ['properties'];
	}

	public function getPropertiesNames(): array
	{
		$names = [];

		foreach ($this->properties as $property) {
			$names[] = $property->name;
		}

		return $names;
	}

}
