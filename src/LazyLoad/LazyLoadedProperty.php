<?php
declare(strict_types=1);

namespace Trick\Model\LazyLoad;

final class LazyLoadedProperty
{
	/** @var Callable */
	public $loader;

	/** @var bool */
	public $isLoaded = false;

	/** @var mixed */
	public $data;

	/** @var string */
	public $type = 'mixed';

	/** @var string */
	public $name = '';
}
