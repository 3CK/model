<?php
declare(strict_types=1);

namespace Trick\Model;

use Nette\Database\Table\Selection;

class Filter
{
	use \Nette\SmartObject;

	/** @var array */
	private $where = [];

	/** @var array */
	private $whereOr = [];

	/** @var string */
	private $order;

	/** @var int */
	private $limit;

	/** @var int */
	private $offset;

	/** @var string */
	private $groupBy;

	/** @var string */
	private $having;

	public function applyTo(Selection &$selection): void
	{
		if (!empty($this->where)) {
			$selection->where($this->where);
		}

		if (!empty($this->whereOr)) {
			$selection->whereOr($this->whereOr);
		}

		if ($this->order) {
			$selection->order($this->order);
		}

		if ($this->groupBy) {
			$selection->group($this->groupBy);
		}

		if ($this->having) {
			$selection->having($this->having);
		}

		if ($this->limit !== null) {
			if ($this->offset) {
				$selection->limit($this->limit, $this->offset);
			} else {
				$selection->limit($this->limit);
			}
		}
	}


	public function where($column, $value = null): self
	{
		if ($value !== null) {
			if (!is_array($this->where)) {
				$this->where = [$this->where];
			}

			$this->where[$column] = $value;
		} elseif (is_array($column)) {
			$this->where = array_merge($this->where, $column);
		} else {
			$this->where[] = $column;
		}

		return $this;
	}


	public function whereOr($column, $value = null): self
	{
		if ($value) {
			$this->whereOr[$column] = $value;
		} elseif (is_array($column)) {
			$this->whereOr = array_merge($this->whereOr, $column);
		} else {
			$this->whereOr[] = $column;
		}

		return $this;
	}


	public function order($order): self
	{
		$this->order = $order;
		return $this;
	}


	public function groupBy($groupBy): self
	{
		$this->groupBy = $groupBy;
		return $this;
	}


	public function having($having): self
	{
		$this->having = $having;
		return $this;
	}


	public function limit($limit, $offset = null): self
	{
		if ($offset) {
			$this->offset = $offset;
		}

		$this->limit = $limit;

		return $this;
	}
}
