<?php
declare(strict_types=1);

namespace Trick\Model;


interface IEntityFactory
{
	public function create(\Nette\Database\Table\ActiveRow $data): Entity;
}

