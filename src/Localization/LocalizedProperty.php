<?php

namespace Trick\Model\Localization;

class LocalizedProperty
{
    /** @var string */
    private $name;

    /** @var array */
    private $values = [];

    /** @var ?string */
    private $currentLocale;


    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addTranslation(string $locale, string $value = null): void
    {
        $this->values[$locale] = $value;
    }

    public function get(string $locale): ?string
    {
        if (!array_key_exists($locale, $this->values)) {
            return null;
        } else {
            return $this->values[$locale];
        }
    }

    public function setLocale(string $locale): void
    {
        $this->currentLocale = $locale;
    }

    public function __toString()
    {
        $return = null;

        if ($this->currentLocale) {
            $return = $this->values[$this->currentLocale];
        } else {
            $return = reset($this->values);
        }

        return (string) $return;
    }
}
